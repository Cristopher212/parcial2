from django.urls import path

from core import views

app_name = "core"

urlpatterns = [
    path('create/ciudades/', views.CreateCiudades.as_view(), name="create_ciudades"),
    path('list/ciudades', views.ListCiudades.as_view(), name="list_ciudades"),
    path('detail/ciudades/<int:pk>/', views.DetailCiudades.as_view(), name="detail_ciudades"),
    path('update/ciudades/<int:pk>/', views.UpdateCiudades.as_view(), name="update_ciudades"),
    path('delete/ciudades/<int:pk>/', views.DeleteCiudades.as_view(), name="delete_ciudades"),

    ####URLS EQUIPOS

    path('create/equipos/', views.CrearEquipo.as_view(), name="create_equipos"),
    path('list/equipos/', views.ListarEquipos.as_view(), name="list_equipos"),
    path('detail/equipos/<int:pk>/', views.DetalleEquipo.as_view(), name="detail_equipos"),
    path('update/equipos/<int:pk>/', views.ActualizarEquipo.as_view(), name="update_equipos"),
    path('delete/equipos/<int:pk>/', views.EliminarEquipo.as_view(), name="delete_equipos"),

    ###URLS ESTADIOS

    path('create/estadios/', views.CrearEstadio.as_view(), name="create_estadios"),
    path('list/estadios/', views.ListarEstadios.as_view(), name="list_estadios"),
    path('detail/estadios/<int:pk>/', views.DetalleEstadio.as_view(), name="detail_estadios"),
    path('update/estadios/<int:pk>/', views.ActualizarEstadio.as_view(), name="update_estadios"),
    path('delete/estadios/<int:pk>/', views.EliminarEstadio.as_view(), name="delete_estadios"),

]