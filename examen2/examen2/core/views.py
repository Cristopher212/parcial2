from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Ciudades, Equipos, Estadios  
from .forms import CiudadesForm, UpdateCiudadesForm  
from .forms import EquiposForm, UpdateEquiposForm
from .forms import EstadiosForm, UpdateEstadiosForm

# Create your views here.

# Create
class CreateCiudades(generic.CreateView):
    template_name = "core/create_ciudades.html"
    model = Ciudades  
    form_class = CiudadesForm  
    success_url = reverse_lazy("core:list_ciudades")

# Retrieve
# List


class ListCiudades(generic.View):
    template_name = "core/list_ciudades.html"
    context = {}

    def get(self, request, *args, **kwargs):
        ciudades = Ciudades.objects.all()  
        self.context = {
            "ciudades": ciudades
        }
        return render(request, self.template_name, self.context)


# Detail
class DetailCiudades(generic.View):
    template_name = "core/detail_ciudades.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        ciudad = Ciudades.objects.get(pk=pk)  
        self.context = {
            "ciudad": ciudad
        }
        return render(request, self.template_name, self.context)

# Update
class UpdateCiudades(generic.UpdateView):
    template_name = "core/update_ciudades.html"
    model = Ciudades  
    form_class = UpdateCiudadesForm  
    success_url = reverse_lazy("core:list_ciudades")

# Delete
class DeleteCiudades(generic.DeleteView):
    template_name = "core/delete_ciudades.html"
    model = Ciudades  
    success_url = reverse_lazy("core:list_ciudades")




### E Q U I P O S


class CrearEquipo(generic.CreateView):
    template_name = "core/create_equipos.html"
    model = Equipos  
    form_class = EquiposForm  
    success_url = reverse_lazy("core:list_equipos")


class ListarEquipos(generic.View):
    template_name = "core/list_equipos.html"
    context = {}

    def get(self, request, *args, **kwargs):
        equipos = Equipos.objects.all()  
        self.context = {
            "equipos": equipos
        }
        return render(request, self.template_name, self.context)


class DetalleEquipo(generic.View):
    template_name = "core/detail_equipos.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        equipo = Equipos.objects.get(pk=pk)  
        self.context = {
            "equipo": equipo
        }
        return render(request, self.template_name, self.context)


class ActualizarEquipo(generic.UpdateView):
    template_name = "core/update_equipos.html"
    model = Equipos  
    form_class = UpdateEquiposForm  
    success_url = reverse_lazy("core:list_equipos")

class EliminarEquipo(generic.DeleteView):
    template_name = "core/delete_equipos.html"
    model = Equipos  
    success_url = reverse_lazy("core:list_equipos")



### E S T A D I O S

class CrearEstadio(generic.CreateView):
    template_name = "core/create_estadios.html"
    model = Estadios  
    form_class = EstadiosForm  
    success_url = reverse_lazy("core:list_estadios")

class ListarEstadios(generic.View):
    template_name = "core/list_estadios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        estadios = Estadios.objects.all()
        self.context = {
            "estadios": estadios
        }
        return render(request, self.template_name, self.context)

class DetalleEstadio(generic.View):
    template_name = "core/detail_estadios.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        estadio = Estadios.objects.get(pk=pk)
        self.context = {
            "estadio": estadio
        }
        return render(request, self.template_name, self.context)

class ActualizarEstadio(generic.UpdateView):
    template_name = "core/update_estadios.html"
    model = Estadios  
    form_class = UpdateEstadiosForm  
    success_url = reverse_lazy("core:list_estadios")

class EliminarEstadio(generic.DeleteView):
    template_name = "core/delete_estadios.html"
    model = Estadios  
    success_url = reverse_lazy("core:list_estadios")