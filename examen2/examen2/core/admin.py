from django.contrib import admin
from .models import Ciudades, Estadios, Equipos


@admin.register(Ciudades)
class CiudadesAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "pais",
    ]

@admin.register(Estadios)
class EstadiosAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "ciudad",
        "capacidad",
    ]

@admin.register(Equipos)
class EquiposAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "liga",
        "fundacion",
    ]

