from django import forms
from .models import Ciudades, Equipos, Estadios

### C I U D A D E S

class CiudadesForm(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = '__all__'


        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre de la ciudad","style": "background-color: gray;"}),
            "pais": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "País de la ciudad", "style": "background-color: gray;"})

        }


class UpdateCiudadesForm(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre de la ciudad" , "style": "background-color: gray;"}),
            "pais": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "País de la ciudad", "style": "background-color: gray;" }),
        }



### E Q U I P O S

class EquiposForm(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = '__all__'  

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del equipo", "style": "background-color: gray;"}),
            "liga": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Liga del equipo", "style": "background-color: gray;"}),
            "fundacion": forms.DateInput(attrs={"type": "date", "class": "form-control" , "style": "background-color: gray;"}),
        }

class UpdateEquiposForm(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = '__all__'  #

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del equipo", "style": "background-color: gray;"}),
            "liga": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Liga del equipo", "style": "background-color: gray;"}),
            "fundacion": forms.DateInput(attrs={"type": "date", "class": "form-control"}),
        }

### E S T A D I O S

class EstadiosForm(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = '__all__'  

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del estadio", "style": "background-color: gray;"}),
            "ciudad": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "capacidad": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Capacidad del estadio", "style": "background-color: gray;"}),
        }

class UpdateEstadiosForm(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = '__all__'  

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del estadio", "style": "background-color: gray;"}),
            "ciudad": forms.Select(attrs={"class": "form-control"}),
            "capacidad": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Capacidad del estadio", "style": "background-color: gray;"}),
        }
