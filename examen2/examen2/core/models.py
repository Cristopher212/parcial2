from django.db import models
from django.contrib.auth.models import User

class Ciudades(models.Model):
    nombre = models.CharField(max_length=100)
    pais = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Estadios(models.Model):
    nombre = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudades, on_delete=models.CASCADE)
    capacidad = models.PositiveIntegerField()

    def __str__(self):
        return self.nombre

class Equipos(models.Model):
    nombre = models.CharField(max_length=100)
    liga = models.CharField(max_length=100)
    fundacion = models.DateField()

    def __str__(self):
        return self.nombre

