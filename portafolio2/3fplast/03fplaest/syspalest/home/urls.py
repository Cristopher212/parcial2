from django.urls import path

from home import views

app_name = "home"

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('', views.Index.as_view(), name = "index_views"),
    path('About/', views.About.as_view(), name = "About_views"),
    path('Index1/', views.Index1.as_view(), name = "index1_views"),
    path('Index2/', views.Index2.as_view(), name = "index2_views"),
    path('Index3/', views.Index3.as_view(), name = "index3_views"),
    path('Index4/', views.Index4.as_view(), name = "index4_views"),
    path('Index5/', views.Index5.as_view(), name = "index5_views"),
    path('Index6/', views.Index6.as_view(), name = "index6_views"),
    path('Index7/', views.Index7.as_view(), name = "index7_views"),
    path('Index8/', views.Index8.as_view(), name = "index8_views"),
    path('Index9/', views.Index9.as_view(), name = "index9_views"),
    path('Index10/', views.Index10.as_view(), name = "index10_views"),
    path('Index11/', views.Index11.as_view(), name = "index11_views"),
    path('Index12/', views.Index12.as_view(), name = "index12_views"),
    path('Index13/', views.Index13.as_view(), name = "index13_views"),
    path('Index14/', views.Index14.as_view(), name = "index14_views"),
    path('Index15/', views.Index15.as_view(), name = "index15_views"),
    path('Index16/', views.Index16.as_view(), name = "index16_views"),
    path('Index17/', views.Index17.as_view(), name = "index17_views"),
    path('Index18/', views.Index18.as_view(), name = "index18_views"),
    path('Index19/', views.Index19.as_view(), name = "index19_views"),
    path('Index20/', views.Index20.as_view(), name = "index20_views"),
    path('new/category/', views.NewCategory.as_view(), name="newcategory"),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),

   
   
]
 