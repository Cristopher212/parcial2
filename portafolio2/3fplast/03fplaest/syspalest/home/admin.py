from django.contrib import admin

# Register your models here.
from home import models



@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin): 
    list_display = [
        "name",
        "user",
        "timestamp",
        "age",
        "status"
    ]

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = [
            "name",
            "user",
            "id",
    ]
    
@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display =[
            "product_name",
            "status",
            "category",
            "id",
      ] 

@admin.register(models.Empresa)
class EmpresaAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "direccion",
        "telefono",
        "id",
    ]

@admin.register(models.Empleado)
class EmpleadoAdmin(admin.ModelAdmin):
    list_display = [
        "user_profile",
        "empresa",
        "puesto",
        "id",
    ]

@admin.register(models.ProductoEmpresa)
class ProductoEmpresaAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "precio",
        "id",
    ]

@admin.register(models.Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "id",
    ]

@admin.register(models.Proyecto)
class ProyectoAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "descripcion",
        "id",
    ]

@admin.register(models.Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "correo",
        "id",
    ]

@admin.register(models.Proveedor)
class ProveedorAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "telefono",
        "id",
    ]

@admin.register(models.Evento)
class EventoAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "descripcion",
        "fecha",
        "id",
    ]

@admin.register(models.Sucursal)
class SucursalAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "direccion",
        "id",
    ]

@admin.register(models.Contacto)
class ContactoAdmin(admin.ModelAdmin):
    list_display = [
        "empresa",
        "nombre",
        "correo",
        "telefono",
        "id",
    ]