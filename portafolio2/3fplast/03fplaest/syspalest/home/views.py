from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
# Create your views here.
from .models import Category
from .forms import UpdateCategoryForm, NewCategoryForm


class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request):
        self.context = {
            "categories": Category.objects.all()
        }
        return render (request, self.template_name, self.context)
    
class DetailCategory(generic.DetailView):
    template_name = "home/detail_category.html"
    model = Category


class UpdateCategory(generic.UpdateView):
    template_name = "home/update_category.html"
    model = Category
    form_class = UpdateCategoryForm
    success_url = reverse_lazy("home:index")



class DeleteCategory(generic.DeleteView):
    template_name = "home/delete_category.html"
    model = Category
    success_url = reverse_lazy("home:index")


class NewCategory(generic.CreateView):
    template_name = "home/newcategory.html"
    model = Category
    form_class = NewCategoryForm
    success_url = reverse_lazy("home:index")




class About(generic.View):
    template_name = "home/About.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index1(generic.View):
    template_name = "home/index1.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index2(generic.View):
    template_name = "home/index2.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index3(generic.View):
    template_name = "home/index3.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index4(generic.View):
    template_name = "home/index4.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index5(generic.View):
    template_name = "home/index5.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index6(generic.View):
    template_name = "home/index6.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index7(generic.View):
    template_name = "home/index7.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index8(generic.View):
    template_name = "home/index8.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index9(generic.View):
    template_name = "home/index9.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index10(generic.View):
    template_name = "home/index10.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)

class Index11(generic.View):
    template_name = "home/index11.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index12(generic.View):
    template_name = "home/index12.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index13(generic.View):
    template_name = "home/index13.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index14(generic.View):
    template_name = "home/index14.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index15(generic.View):
    template_name = "home/index15.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index16(generic.View):
    template_name = "home/index16.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index17(generic.View):
    template_name = "home/index17.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index18(generic.View):
    template_name = "home/index18.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index19(generic.View):
    template_name = "home/index19.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)
    
class Index20(generic.View):
    template_name = "home/index20.html"
    context = {}

    def get(self, request):
        return render (request, self.template_name, self.context)