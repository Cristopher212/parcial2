from django import forms
from .models import Category

class UpdateCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "user",
            "name",
            "status"
        ]


class NewCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "user",
            "name",
            "status"
        ]
